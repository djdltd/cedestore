/*
 * AddTrustedContactScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;


import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import javax.microedition.pim.*;
/**
 * 
 */
class SingleNoteScreen extends MainScreen {    
    
    private EditField _eftitle;    
    private EditField _efcontents;
    
    private boolean _updatedrecord = false;
    private boolean _recordsaved = false;
    
    private String _tempid = "";
    private boolean _tempidpresent = false;
    
    private SingleNote _note;
    
    private MenuItem _saveMenuItem = new MenuItem("Save", 100, 10) 
    {
        public void run()
        {

            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            setLocalnote();
            
            _recordsaved = true;
            
            if (validateForm() == true) {
                UiApplication uiapp = UiApplication.getUiApplication();
                uiapp.popScreen(uiapp.getActiveScreen());
            }                                   
        }
    };
    
    private MenuItem _cancelMenuItem = new MenuItem("Cancel", 100, 10) 
    {
        public void run()
        {       
            _recordsaved = false;
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());                                               
        }
    };

    SingleNoteScreen()
    {    
        setTitle(new LabelField("Encrypted Item" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
        
        //_fromaddress = new EditField("From:", "", MAX_PHONE_NUMBER_LENGTH, EditField.FILTER_PHONE);
        _eftitle = new EditField("Title: ", "");
        _efcontents = new EditField("", "");
                        
        
        add(_eftitle);
        add(new SeparatorField());
        add(_efcontents);
        
        addMenuItem(_saveMenuItem);        
        addMenuItem(_cancelMenuItem);
    }
    
    public SingleNote getNote()
    {
        return _note;
    }
    
    public boolean validateForm ()
    {
        if (_eftitle.getText().length() < 2) {
            Dialog.alert("Please enter a valid title.");
            return false;
        }
                
        return true;
    }
    
    public void setNote (SingleNote note)
    {
        _note = note;
        setLocalform();
        
        _tempid = _note._id;
        _tempidpresent = true;
    }
    
    public boolean isUpdatedrecord()
    {
        return _updatedrecord;
    }
    
    public boolean isSaved()
    {
        return _recordsaved;
    }
    
    public void setLocalnote ()
    {
        _note = new SingleNote();
        Date dtCurrent = new Date();
                
        if (_tempidpresent == true) {           
            _updatedrecord = true;
            _note._id = _tempid;           
        } else {
            _updatedrecord = false;
            _note._id = dtCurrent.toString();            
        }
        
        _note._title = _eftitle.getText();
        _note._contents = _efcontents.getText();

    }
    
    public void setLocalform()
    {
        _eftitle.setText(_note._title);
        _efcontents.setText(_note._contents);        
    }
    
    public boolean onSave()
    {
        //Dialog.alert("Auto Saved!");
        setLocalnote();
        _recordsaved = true;
        
        return true;
    }
    
    public void close()
    {
        if (validateForm() == true) {
            super.close();
        }                                
    }
    
} 
