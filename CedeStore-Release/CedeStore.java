/*
 * CedeStore.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;


import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.Displayable;
import java.util.*;
import java.io.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.util.Persistable;

/**
 * 
 */
class CedeStore extends UiApplication {
    
    private PersistentObject _persisttrial;
    private TrialInfo _trialinfo;
    private boolean _trialversion = true;
    private UserSettingsScreen _userSettingsscreen = new UserSettingsScreen();
        
    private ListField _listField;   
    private Vector _encryptednotes = new Vector();
    private Vector _cachedtitles = new Vector();
    private PersistentObject _persist;
    private SingleNoteScreen _singlenotescreen;
    
    private String strDemomessage = "This trial version of " + GlobalAppProperties._appname + " has expired. Please contact CedeSoft for further information.";        
    
    private MenuItem _optionsMenuItem = new MenuItem("Options", 100, 10) 
    {                
        public void run()
        {
            _userSettingsscreen = new UserSettingsScreen();
            
            UiApplication.getUiApplication().pushModalScreen(_userSettingsscreen);
            
        }
    };
    
    private MenuItem _aboutMenuItem = new MenuItem("About", 100, 10) 
    {                
        public void run()
        {            
            if (_trialversion == true) {
                Dialog.alert( GlobalAppProperties._appnamewithversion + ".7.\n\n7 Day Trial\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
            } else {
                Dialog.alert(GlobalAppProperties._appnamewithversion + ".99.\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
            }                
        }
    };
    
    private MenuItem _deleteMenuItem = new MenuItem("Delete", 100, 10) 
    {
        public void run()
        {
            if (Dialog.ask(Dialog.D_YES_NO, "Delete Encrypted Item?") == Dialog.YES) {
                int index = _listField.getSelectedIndex();
                
                _encryptednotes.removeElementAt(index);
                
                saveNotelist();
                
                cacheNotetitles();
                
                _listField.setSize(_encryptednotes.size());
            }            
        }
    };
    
    private MenuItem _addMenuItem = new MenuItem("New Encrypted Item", 100, 10) 
    {
        public void run()
        {
            if (IsExpired() == false) {
            
                UserSettings mysettings = new UserSettings();
                    
                _userSettingsscreen = new UserSettingsScreen();
                mysettings = _userSettingsscreen.getUserSettings();          
                
                if (mysettings._istrial.compareTo("yes") == 0) {
                    if (_encryptednotes.size() >=3) {
                        Dialog.alert("The Trial version of " + GlobalAppProperties._appname + " is restricted to 3 encrypted items. Please contact CedeSoft for purchase information.");
                        return;
                    }
                }
                
                SingleNote note;
                _singlenotescreen = new SingleNoteScreen();            
                UiApplication.getUiApplication().pushModalScreen(_singlenotescreen);
                
                
                if (_singlenotescreen.isSaved() == true)
                {
                    if (_singlenotescreen.isUpdatedrecord() == false) {
                        // A new  record was added
                        SingleNote encnote = new SingleNote();
                        
                        note = new SingleNote();
                        note = _singlenotescreen.getNote();
                        
                        encnote._title = Encryption.EncryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), note._title);
                        encnote._contents = Encryption.EncryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), note._contents);
                        
                        
                        _encryptednotes.addElement(encnote);
                        
                        cacheNotetitles();
                        _listField.setSize(_encryptednotes.size());
                        saveNotelist();                    
                        
                    } else {
                        // An existing note record was updated
                    }
                }
            } else {
                Dialog.alert(strDemomessage);
            }
        }
    };
    
    private MenuItem _enterlicenseMenuItem = new MenuItem("Enter License Key", 100, 10) 
    {                
        public void run()
        {   
            UserSettings mysettings = new UserSettings();
                          
            UiApplication.getUiApplication().pushModalScreen(new LicenseScreen());                       
            
            _userSettingsscreen = new UserSettingsScreen();
            mysettings = _userSettingsscreen.getUserSettings();                                                 
            
            if (mysettings._istrial.compareTo("yes") == 0) {
                _trialversion = true;
                loadTrialinfo();
            } else {
                _trialversion = false;
            }
        }
    };
    
    CedeStore() {    
            
            CedeStoreMainScreen screen = new CedeStoreMainScreen();
            pushScreen(screen);
    }

    public static void main(String[] args)
    {
        
        // Create a new instance of the application and start 
        // the application on the event thread.
        CedeStore cedestore  = new CedeStore();
        cedestore.enterEventDispatcher();
    }


    public void activate()
    {
           LicenseandPasswordCheck();
    }

    public void LicenseandPasswordCheck()
    {     
        UiApplication.getUiApplication().invokeLater( new Runnable()
        {
            public void run ()
            {
                
                UserSettings mysettings = new UserSettings();
                
                _userSettingsscreen = new UserSettingsScreen();
                mysettings = _userSettingsscreen.getUserSettings();          
                
                if (mysettings._licenseentered.compareTo("yes") != 0) {
                    UiApplication.getUiApplication().pushModalScreen(new LicenseScreen());                       
                    
                    _userSettingsscreen = new UserSettingsScreen();
                    mysettings = _userSettingsscreen.getUserSettings();                                                 
                    
                    if (mysettings._istrial.compareTo("yes") == 0) {
                        _trialversion = true;
                        loadTrialinfo();
                    } else {
                        _trialversion = false;
                    }
                                        
                } else {
                    if (mysettings._istrial.compareTo("yes") == 0) {
                        _trialversion = true;
                        loadTrialinfo();
                    } else {
                        _trialversion = false;
                    }
                }
                
                // Now check if we need to show the password prompt screen
                pushModalScreen(new PasswordScreen());                                    

                if (GlobalAppVars._factoryreset == true) {
                    _encryptednotes.removeAllElements();
                    saveNotelist();
                }

                cacheNotetitles();
                _listField.setSize(_encryptednotes.size());
            }
        });
    }
    
    public void editNoteitem()
    {
            int index = _listField.getSelectedIndex();
            
            SingleNote note;
            _singlenotescreen = new SingleNoteScreen();
            
            note = new SingleNote();            
            note = (SingleNote) _encryptednotes.elementAt(index);
            
            SingleNote decnote = new SingleNote();
            decnote._title = Encryption.DecryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), note._title);
            decnote._contents = Encryption.DecryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), note._contents);            
            
            _singlenotescreen.setNote(decnote);
                        
            UiApplication.getUiApplication().pushModalScreen(_singlenotescreen);
            
            if (_singlenotescreen.isSaved() == true)
            {
                if (_singlenotescreen.isUpdatedrecord() == true) {
                    // A single note item was updated
                    note = new SingleNote();
                    note = _singlenotescreen.getNote();                    
                    
                    SingleNote encnote = new SingleNote();
                    encnote._title = Encryption.EncryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), note._title);
                    encnote._contents = Encryption.EncryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), note._contents);
                    
                    _encryptednotes.setElementAt(encnote, index);
                    
                    cacheNotetitles();
                    _listField.setSize(_encryptednotes.size());
                    saveNotelist();
                                        
                } else {
                    // An existing trusted contact record was updated
                }
            }
    }
    
    public void cacheNotetitles()
    {
        // Decrypts all of the note titles in the encrypted notes list
        // and stores them temporarily decrypted in a cache list for faster access by the list
        // view
        
        _cachedtitles.removeAllElements();
        SingleNote curNote = new SingleNote();
        String curTitle = "";
        
        int a = 0;
        
        if (GlobalAppVars._encryptedpasswordpresent == true) {
            for (a=0;a<_encryptednotes.size();a++) {
                curNote = (SingleNote) _encryptednotes.elementAt(a);
                
                curTitle = Encryption.DecryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), curNote._title);
                
                _cachedtitles.addElement(curTitle);
            
            }
        }
    }
    
    public boolean IsExpired ()
    {
        if (_trialversion == true) {
        
            Date dtCurrent = new Date();            
            Date dtExpire = new Date(_trialinfo.expiretime);
            
            if (dtCurrent.getTime() > dtExpire.getTime()) {
                return true;
            } else {
                return false;
            }
        
        } else {
            return false;
        }        
    }
    
    public void loadTrialinfo()
    {
        long KEY = GlobalAppProperties._trialinfoKEY;        
        _persisttrial = PersistentStore.getPersistentObject( KEY );
        _trialinfo = (TrialInfo) _persisttrial.getContents();
        if( _trialinfo == null ) {
            _trialinfo = new TrialInfo();
            
            // Set the trial expire time                        
            _persisttrial.setContents( _trialinfo );
            
            Date dtCurrent = new Date();        
            long expiretime = dtCurrent.getTime();                            
            expiretime = expiretime + (((60000) * 60) * 24) * 7;
            
            _trialinfo.expiretime = expiretime;            
            _persisttrial.commit();            
        }
    }

    public void saveNotelist()
    {
        _persist.commit();
    }

    public void loadNotelist ()
    {
         // Hash of "CedeStore".
         //7b25c9e033a66dc3010c12604d7d5c81
         long KEY =  0xb25c9e033a66dc30L;
         _persist = PersistentStore.getPersistentObject( KEY );
                
        _encryptednotes = (Vector) _persist.getContents();
        if( _encryptednotes == null ) {
            _encryptednotes = new Vector();
            _persist.setContents( _encryptednotes );
            //persist.commit()
        }                     
    }

    private class CedeStoreMainScreen extends MainScreen implements ListFieldCallback {
                
        // Constructor
        private CedeStoreMainScreen()
        {
            
            setTitle(new LabelField(GlobalAppProperties._appname, LabelField.USE_ALL_WIDTH));
            
            _listField = new ListField();        
            _listField.setCallback( this );
            
            add(_listField);
            addMenuItem(_addMenuItem);
            addMenuItem(_deleteMenuItem);
            addMenuItem(_enterlicenseMenuItem);
            addMenuItem(_aboutMenuItem);
            
            loadNotelist();
            //_listField.setRowHeight ((_listfontnormal.getHeight()*2)+1);
            _listField.setSize(_encryptednotes.size());
        }
        
        private SingleNote getSelectedNote() 
        {
            int selectedIndex = _listField.getSelectedIndex();
            
            if ( selectedIndex == -1 ) 
            {
                return null;
            }
                        
            return (SingleNote) _encryptednotes.elementAt( selectedIndex );
        }    
        
        public boolean invokeAction(int action)
        {        
            switch(action)
            {
                case ACTION_INVOKE: // Trackball click.
                    SingleNote note = getSelectedNote();
                    
                    if (note != null) {                                                                       
                            //if (smsmessage.getType() == 0) {
                                //pushScreen(new ViewSMSScreen(0));
                            //}
                            
                            //if (smsmessage.getType() == 1) {
                                //pushScreen(new ViewSMSScreen(1));
                            //}
                        
                            //_fromaddress.setText(smsmessage.getAddress());
                            //_frommessage.setText(smsmessage.getMessage());
                            if (IsExpired() == false) {
                                editNoteitem(); 
                            } else {
                                Dialog.alert(strDemomessage);
                            }
                            
                    }
                    return true; // We've consumed the event.
            }   
            
            return  super.invokeAction(action);
        }
        
        public void drawListRow(ListField listField, Graphics graphics, int index, int y, int width) 
        {
            if(listField == _listField && index < _encryptednotes.size())
            {
                if (GlobalAppVars._encryptedpasswordpresent == true) {
                    
                    if (index < _cachedtitles.size()) {
                        
                        String strTitle = (String) _cachedtitles.elementAt(index);
                        
                        graphics.drawText(strTitle, 0, y, 0, width);
                    }
                    
                    //SingleNote item = (SingleNote) _encryptednotes.elementAt(index);
                    
                    //String strTitle = Encryption.DecryptTextEx(Encryption.Instantdecrypt(GlobalAppVars._encryptedpassword), item._title);                                                                            
                    //String strEnctitle = item._title;
                    //graphics.drawText(Fullname, 0, y, 0, width);  
                    //graphics.drawText(strTitle, 0, y, 0, width);
                    
                    
                    
                    //graphics.drawText (item._trustedstatus, 0, y+_listfontnormal.getHeight(), 0, width);
                    //graphics.drawLine (0, y+(_listfontnormal.getHeight()*2), width, y+(_listfontnormal.getHeight()*2));
                }                                
            }
        }
    
        /**
        * @see net.rim.device.api.ui.component.ListFieldCallback#get(ListField , int)
        */
        public Object get(ListField listField, int index)
        {
            if(listField == _listField)
            {
                // If index is out of bounds an exception will be thrown, but that's the behaviour
                // we want in that case.
                return _encryptednotes.elementAt(index);
            }
            
            return null;
        }
        
        /**
        * @see net.rim.device.api.ui.component.ListFieldCallback#getPreferredWidth(ListField)
        */
        public int getPreferredWidth(ListField listField) 
        {
            // Use all the width of the current LCD.
            return getWidth();
        }
    
        /**
        * @see net.rim.device.api.ui.component.ListFieldCallback#indexOfList(ListField , String , int)
        */   
        public int indexOfList(ListField listField, String prefix, int start) 
        {
            return -1; // Not implemented.
        }
        
    }


} 
