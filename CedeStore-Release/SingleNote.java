/*
 * SingleNote.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;

import java.util.*;
import net.rim.device.api.util.Persistable;


/**
 * 
 */
class SingleNote implements Persistable {
    
    public String _id = "";
    public String _title = "";
    public String _contents = "";
    
    SingleNote() {    
    
    }
    
    
} 
